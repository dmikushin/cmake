# Distributed CMake abstraction

CMake supports "launchers" for distributed compilation. A launcher prepends the supported
compiler or linker and acts as a command line "wrapper" script. Launcher is added only if the user
defines the corresponding environment variables manually, for example CXX_LINKER_LAUNCHER.

Unlike the "launchers" approach above, this patch prepends all supported compilers and linkers
with a "distcmake" wrapper by default, with no further user configuration needed. In order to
enable this mode, a user should simply invoke CMake like this:

export CMAKE_DISTRIBUTED_BUILD=1
cmake .

Then, CMake will issue generator rules, that shall include "distcmake" wrapper:

```
[ 50%] Linking CXX executable test
/home/marcusmae/amd/cmake_distributed/install/bin/cmake -E cmake_link_script CMakeFiles/test.dir/link.txt --verbose=1
distcmake /usr/bin/c++ -rdynamic CMakeFiles/test.dir/test.cpp.o -o test
```

The "distcmake" implementation is to be provided separately. The trivial stub could just
fallback to the traditional compilation:

```bash
#!/usr/bin/bash
$@
```

